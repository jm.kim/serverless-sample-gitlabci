'use strict';
require('dotenv').config();
const lib = require('./lib');

module.exports.retrieveFile = (event, context, cb) => {
  const query = event.query;
  const stage = process.env.STAGE;
  const region = process.env.REGION;

  const functionName = `${region}-${stage}-file-manager-retrieveFile`;
  const lambda = new lib.Lambda();

  lambda.invokeFunction(functionName, { query }, false)
  .then((result) => {
    cb(null, result);
  })
  .catch((error) => {
    cb(JSON.stringify(error), null);
  });
};
